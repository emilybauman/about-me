## Week of August 8th, 2022 to August 12th, 2022

## 15.3 Priorities
- [UX Transition Plan for Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1998)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] Skip Level: Christie and UX Enablement/Growth
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Release Group Weekly [REC]
 - [x] UX Buddy session: Emily & Gina
 - [x] UX Weekly Call
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Rayana & Emily 1:1
 - [x] Design Pair Coffee Chat ☕
 
### Tasks for Release:
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Close off any final tasks for [Task 2: Prepare 1-2 JTBDs for the Release Orchestration (RO) category.](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Get started on a few [issues for 15.4](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/153#note_1042741615).  
 - [ ] Close off final tasks for the [UX Roadmap for Release](https://gitlab.com/gitlab-org/gitlab/-/issues/364527).
 - [x] Clean up [Show tags related to deployed commit on Environments views](https://gitlab.com/gitlab-org/gitlab/-/issues/15419).
 - [x] Finish off [Usability fixes to release assets section of new release page](https://gitlab.com/gitlab-org/gitlab/-/issues/367897).
 - [x] Provide Will with Feedback on [2023 Q3 - KR4: Usability Benchmarking for Release](https://gitlab.com/groups/gitlab-org/-/epics/7899#note_1054141695).

### Other Tasks:
 - [ ] Finish up work on [redesigning the login page](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/91673). 
 - [x] Finish up reading for [Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1864).
 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
