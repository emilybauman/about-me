## Week of November 25th, 2024 to November 29th, 2024

## 17.7 Priorities
- AI Settings 
- Q
- Beautifying our UI

### Key Meetings
 - [x] [REC] UX Call
 - [x] Emily / Libor (design pair)
 - [x] Emily | Tiger Coffee Chat
 - [x] AI Framework - UX & FE Sync
 - [x] Anna & Emily Coffee Chat
 - [x] Emily/Veethika ☕️

### Tasks for AI Settings:
 - [x] Review MRs as they come in. 
 - [ ] Discuss what is next with AI Framework. 

### Tasks for Q: 
 - [x] Review Jackie's videos and roadmap. 
 - [ ] Review outstanding designs. 
      - [x] [Support GitLab Instance ID auth scoping in onboarding flow](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/233)
      - [x] [Add Instance ID](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/105#note_2224396796)

### Other Tasks:
 - [x] Start planning for Beautufying our UI in 17.7. 
 - [ ] Shift Nudge Tasks
      - [ ] Finish watching videos. 
      - [ ] Finish Week 4 & 5 Homework. 