## Week of February 5th, 2024 to February 9th, 2024

## 16.9 Priorities
- [Environments 16.9 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/53)

### Key Meetings
 - [x] Emily / Austin <> Design Pair
 - [x] Emily / Viktor weekly
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] UX Call
 - [x] Rayana & Emily 1:1

 
### Tasks for Environments:
 - [ ] Finish designs for [Environment logs and events streaming visualisation - Design](https://gitlab.com/gitlab-org/gitlab/-/issues/391514) and [Create Empty States for Deployment Details Page](https://gitlab.com/gitlab-org/gitlab/-/issues/439413).
 - [ ] Start on [Show the release process related to environments](https://gitlab.com/gitlab-org/gitlab/-/issues/432671).
 - [ ] Continue work on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839) again. 
 - [x] Finish planning for [16.10](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/66). 

### Other Tasks:
 - [ ] Template [Competitor Evaluation FigJam File](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2416). 
