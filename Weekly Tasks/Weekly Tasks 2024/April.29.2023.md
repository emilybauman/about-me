## Week of April 29th, 2024 to May 3rd, 2024

## 17.0 Priorities
- [Import and Integrate 17.0 Plan](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17672)
- [Environments 17.0 Plan](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/73)

### Key Meetings
 - [x] Solution validation session
 - [x] Solution validation session
 - [x] Solution validation session
 - [x] Emily / Viktor weekly
 - [x] Solution validation session
 - [x] [REC] UX Call
 - [x] Sam | Emily Coffee Chat
 - [x] Divya | Emily Onboarding Buddy Chat
 - [x] Emily / Marcel skip level
 - [x] ☕ Hunter & Emily
 
### Tasks for Import and Integrate:
 - [ ] Finish CSV Proposal for [User contribution mapping](https://gitlab.com/groups/gitlab-org/-/epics/12378) and finalize any existing flows.
 - [x] Conduct moderated and unmoderated testing for [Solution Validation: User Mapping](https://gitlab.com/gitlab-org/ux-research/-/issues/2953).
 - [x] Plan for 17.1.
 - [ ] If time allows, take a look at the following issues:
      - [ ] [Improve discoverability of import results](https://gitlab.com/gitlab-org/gitlab/-/issues/431178).

### Tasks for Environments:
 - [x] Work on [Think through the Flux bootstrap command UX and related arguments](https://gitlab.com/gitlab-org/gitlab/-/issues/454546).
 - [x] Create an issue for the remaining two JTBDs and review feedback. 
 - [x] Plan for 17.1.

### Other Tasks:
 - [x] UX Buddy Tasks. 
 - [x] Coverage for Rayana. 
 - [x] Go through research framework. 