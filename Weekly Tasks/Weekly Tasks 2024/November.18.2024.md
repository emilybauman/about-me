## Week of November 18th, 2024 to November 22nd, 2024

🌴 Slightly Short Week - OOO Monday for PTO.

## 17.7 Priorities
- AI Settings 
- Q
- Beautifying our UI

### Key Meetings
 - [x] Emily / Viktor - Coffee Chat
 - [x] Patrick / Emily Design Sync
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Andrei & Emily Monthly ☕
 - [x] AI Framework - UX & FE Sync
 - [x] Jackie / Emily Sync
 - [x] AI UX Share (AMER)
 - [x] Emily / Pini catch up
 - [x] Support <> AI Framework Sync
 - [x] Shift Nudge catch up
 - [x] Data Science section UX meetup
 - [x] Emily / Jacki
 - [x] Emily / Jay ☕️
 - [x] Emily / Libor (design pair)

### Tasks for AI Settings:
 - [x] Catch up from PTO and craft plan for next week. 

### Tasks for Q: 
 - [x] Catch up from PTO and craft plan for next week. 

### Other Tasks:
 - [ ] Start planning for Beautufying our UI in 17.7. 
 - [ ] Shift Nudge Tasks
      - [ ] Finish Week 4 & 5 Homework. 