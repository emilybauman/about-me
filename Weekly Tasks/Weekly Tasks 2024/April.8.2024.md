## Week of April 8th, 2024 to April 12th, 2024

I am out of office on vacation in Ireland. 🌴

Please contact my manager, [Rayana](https://gitlab.com/rayana) for anything urgent.
I will be back on Tuesday, April 16 2024.

