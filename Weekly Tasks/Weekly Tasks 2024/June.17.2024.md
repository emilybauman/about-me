## Week of June 17th, 2024 to June 21st, 2024

🌴 Slightly Short Week - OOO Friday for a Vacation.

## 17.2 Priorities
- AI Settings

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Taylor / Emily AI Settings Sync
 - [x] Lindsey & Emily ☕
 - [x] Option 1: FY25-Q2 GitLab Assembly all-company meeting
 - [x] Product All-Team Meeting [AMER] [REC] (Quarterly)
 - [x] Pam & Emily coffee chat
 - [x] Will/Emily Research Sync
 - [x] [Americas] CI/CD UX Meeting
 - [x] Anna & Emily Coffee Chat
 - [x] Emily & Katie - AI Onboarding Sync
 - [x] Nick & Emily - Research Sync
 - [ ] Rayana & Emily 1:1
 - [ ] Suzanne & Emily ☕

### Tasks for AI Settings:
 - [x] Continue meeting the team through Coffee Chats. 
 - [x] Get a plan organized for [Solution Validation: Unified Duo Settings](https://gitlab.com/gitlab-org/ux-research/-/issues/3080).
 - [x] Review any additional work in flight.

### Other Tasks:
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).
 - [x] Finish Mid Year Self Review.