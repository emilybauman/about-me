## Week of August 12th, 2024 to August 16th, 2024

## 17.3 Priorities
- AI Settings and Duo Trials

### Key Meetings
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Anna & Emily Coffee Chat
 - [x] Andrew & Emily Monthly ☕
 - [x] Emily / Paul
 - [x] AI Settings - Content Brainstorm
 - [x] Divya | Emily Onboarding Buddy Chat
 - [x] Becka/ Emily ☕️ chat
 - [x] Rayana & Emily 1:1
 - [x] [Americas] CI/CD UX Meeting
 - [x] Emily / Pini catch up
 - [x] Support <> AI Framework Sync

### Tasks for AI Settings:
 - [x] Finalize content and update issue for [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
 - [x] Work on illustration for [[UX] UX and copy requirements for combined Ultimate + Duo Enterprise trial registration](https://gitlab.com/gitlab-org/gitlab/-/issues/468027/).
 - [ ] Finish [UX New users: Add Duo Enterprise trial creation to existing Ultimate trial new user registration flow for Gitlab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/473814). 

### Other Tasks:
 - [ ] Review Environments and Import and Integrate design needs if capacity allows. 
    - [x] [Add a sticky head to Environment Details Page](https://gitlab.com/gitlab-org/gitlab/-/issues/439766).
    - [ ] [Add a description text field to environment details](https://gitlab.com/gitlab-org/gitlab/-/issues/450944).
    - [x] Help Anna with some minor issues. 
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).
 - [x] Shift Nudge Tasks
      - [x] Buy Course.
      - [x] Expense 50% of Course.
      - [x] Finish [Initial Setup](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2642#initial-setup) and [Prerequities](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2642#prerequisites). 