## Week of June 24th, 2024 to June 28th, 2024

🌴 Slightly Short Week - OOO Monday and Wednesday for Vacation.

## 17.2 Priorities
- AI Settings

### Key Meetings
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Jeff & Emily ☕
 - [x] Emily / Nico Coffee Chat
 - [x] Emily & Gina sync

### Tasks for AI Settings:
 - [x] Continue meeting the team through Coffee Chats. 
 - [x] Finish screener and review feedback on discussion guide for [Solution Validation: Unified Duo Settings](https://gitlab.com/gitlab-org/ux-research/-/issues/3080).
 - [x] Review [Early Access (beta) program setting - ux only](https://gitlab.com/gitlab-org/gitlab/-/issues/460093).
 - [x] Review any additional in flight work. 

### Other Tasks:
 - [ ] UX Buddy Tasks. 
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).
 - [x] Set up Manulife. 