## Week of February 12th, 2024 to February 16th, 2024

## 16.9 Priorities
- [Environments 16.9 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/53)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Rayana & Emily 1:1
 - [x] CI/CD UX Meeting
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily & Gina sync
 - [x] Rant/ ☕️ chat
 
### Tasks for Environments:
 - [ ] Get started on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839) again. 
 - [ ] Finish [Environment logs and events streaming visualisation - Design](https://gitlab.com/gitlab-org/gitlab/-/issues/391514).
 - [x] Record [16.10 Kickoff](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/66).
 - [ ] Work on [Show the release process related to environments](https://gitlab.com/gitlab-org/gitlab/-/issues/432671) if time permits. 

### Other Tasks:
 - [ ] Template [Competitor Evaluation FigJam File](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2416). 
 - [x] UX Buddy Tasks.
 - [x] Buy and expense new headphones.
 - [ ] Finish first version of UX Showcase Presentation. 
