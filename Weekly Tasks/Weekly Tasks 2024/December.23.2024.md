## Week of December 23th, 2024 to December 27th, 2024

I am out of office for the Holidays. 🌴

Please contact my manager, [Jacki](https://gitlab.com/jackib) for anything urgent.
I will be back on Thursday, January 2nd, 2025. 
