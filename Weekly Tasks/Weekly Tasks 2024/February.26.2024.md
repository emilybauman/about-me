## Week of February 26th, 2024 to March 1st, 2024

## 16.10 Priorities
- [Environments 16.10 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/66)

### Key Meetings
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Emily / Austin <> Design Pair
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] Rayana & Emily 1:1
 - [x] IBM Design Studio Experience
 - [x] CI/CD UX Meeting
 - [x] Pam & Emily coffee chat
 
### Tasks for Environments:
 - [x] Finish tasks for first step of [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839) again. 
 - [x] Get an iteration done for [Add a sticky head to Environment Details Page](https://gitlab.com/gitlab-org/gitlab/-/issues/439766).
 - [ ] Work on [Show the release process related to environments](https://gitlab.com/gitlab-org/gitlab/-/issues/432671) if time permits. 

### Other Tasks:
 - [ ] Template [Competitor Evaluation FigJam File](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2416). 
 - [x] UX Buddy Tasks. 
 - [x] Plan for [16.11](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/67).
 - [x] Complete SAFE Training for Summit. 
 - [ ] Complete tasks for [Actionable Insights Workshop](https://gitlab.com/gitlab-org/ux-research/-/issues/2901).
 - [x] Complete Skills Matrix.
 - [x] Open FY25 IGP Issue.  
