## Week of December 20th, 2021 to December 24th, 2021

Short Week - Off December 24th

## 14.7 Priorities
- [Activation Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/530)
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/528) this milestone.

### Key Meetings:
 - [ ] 1:1 with Jacki
 - [ ] UX Holiday Event
 - [ ] UX Weekly
 - [ ] Aquisitions Team Sync
 - [ ] 1:1 with Jensen

### Tasks for Activation:
 - [ ] Finish up any additional work on [Test requiring credit card for all new namespaces](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/533)
 - [ ] Start work on [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465)

### Tasks for Expansion:
 - [ ] Finish first iteration of [Experiment UX: during cart abandonment offer trial and talk to sales option
](https://gitlab.com/gitlab-org/gitlab/-/issues/343179)
- [ ] Start on [When namespace reaches user limit alert Owners via banners and email](https://gitlab.com/gitlab-org/gitlab/-/issues/348696)
- [ ] If time permits, start on [Indicate how many seats are remaining when namespace approaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/348694)

### Other Tasks:
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
