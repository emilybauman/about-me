## Week of November 1st, 2021 to November 5th, 2021

## 14.5 Priorities
 - [Expansion Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/447)
 - [Activation Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/455)

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] Jacki's UX Team Meeting
 - [x] 1:1 with Matej
 - [x] Growth Weekly
 - [x] UX Weekly
 - [ ] Growth Engineering Weekly
 - [x] Activation Team Sync
 - [ ] Growth Coffee and Learn
 - [x] Sync with Gayle
 - [ x] Emily and Andy Design Pair 
 - [x] Async with Kevin
 - [x] Expansion Mid-Milestone Catchup
 - [x] Reforge: Growth Models

### Tasks for Activation:
 - [x] Continue Credit Card Validation work on [When pipeline fails due to cc verification failure, Create a persistent header in the UI for that user](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/464) and [Add "verify your identity" flow to first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/452).
 - [x] Finalize flow of [Update continuous onboarding to use CI/CD walkthrough](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/446).
 - [ ] Facilitate feedback for [Create a marketing header when user is on GitLab and not logged in](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/448).
 - [x] Start looking at work related to [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465).

### Tasks for Expansion:
 - [ ] Work with Gayle and Nick for next steps on [How are non-invited users finding their team's namespace + groups/projects when they create new accounts?](https://gitlab.com/gitlab-org/gitlab/-/issues/340755).
 - [x] Create issue and get started on initial work for [Invited User UX Scorecard](https://gitlab.com/gitlab-org/growth/product/-/issues/1702#note_721506666).

### Other Tasks:
 - [ ] Continue Work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [x] Finish up Reforge lessons for the week - Growth Models - Growth Series
 - [ ] Start Reforge lessons for next week - User Psychology - Growth Series
 - [x] Take a look at [FY22-Q4 KR: Evaluate the experience of 7 product categories and make recommendations to improve learnability](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12593)
 - [ ] Finish up my talent assessment for next week for [Talent Assessment FY22 - Growth/Fulfillment](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1777) 

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] Urdu Lessons
 - [ ] Prep for November Vacation
