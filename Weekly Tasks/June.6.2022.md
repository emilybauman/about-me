## Week of June 6th, 2022 to June 10th, 2022

🌴 - Short Week - Out of office for a camping trip, June 9th and 10th.

## 15.1 Priorities
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/637)

### Key Meetings:
 - [x] Designer pair: Becka/ Emily
 - [x] GitLab Q1 - Fiscal 2023 Earnings Call
 - [x] Coffee Chat ☕ Jannik/Emily
 - [x] Jacki's UX Team (FG) Meetup [REC]
 - [x] Managing Your Account for Gitlab Non US Participants
 - [x] UX Weekly Call
 - [x] 1:1:1 - Transition Meeting
 - [x] Emily / Kevin - Sync
 - [x] Activation Team Sync
 - [x] UX Showcase
 - [x] Emily / Gayle/Sam Bi-Weekly
 
### Tasks for Activation:
 - [ ] Finish Up [Determine barriers to entry at signup (from Arkose risk score)](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/9).
 - [ ] Close off any outstanding Anti-Abuse Tasks.

### Tasks for Expansion:
 - [x] Close off any outstanding Conversion/Expansion Tasks.

### Other Tasks:
 - [x] Start on [Beautifying Our UI 15.2](https://gitlab.com/gitlab-org/gitlab/-/issues/362122).
 - [x] Work on transition plan into Release.
 
### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
