## Week of March 21st, 2022 to March 25th, 2022

- Family and friends day on March 25th ☀️

## 14.10 Priorities
- [Activation/Adoption Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/619)
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/618)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/621)

### Key Meetings:
 - [x] Growth Weekly
 - [x] UX Weekly
 - [x] 1:1 with Matej
 - [ ] Async with Kevin
 - [ ] Reforge Kickoff
 - [ ] 1:1 with Jacki
 - [ ] 1:1 with Nick
 - [ ] Design Pair with Becka

### Tasks for Activation:
 - [ ] Finish off final touches to [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93).
 - [ ] Get a good chunk of work done on [Add walkthrough/wizard for the standard CI template](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/572).

### Tasks for Expansion:
 - [x] Start on [On members page add a link to the usage quota seats page](https://gitlab.com/gitlab-org/gitlab/-/issues/353226).
 - [ ] If time permits, also take a look at [UX: Ability for admin to view and control who can access their namespace - active trial](https://gitlab.com/gitlab-org/gitlab/-/issues/354536).

### Other Tasks:
 - None

### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
