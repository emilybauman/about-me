## Week of May 16th, 2022 to May 20th, 2022

## 15.0 Priorities
- Activation/Adoption Planning Issue: None for this Milestone 
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/629)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/628)

### Key Meetings:
 - [ ] Coffee Chat with Rayana
 - [ ] UX Weekly Call
 - [ ] 1:1 with Matej
 - [ ] UX Weekly Call
 - [ ] Growth Conversion Team Meeting
 - [ ] Coffee Chat with Taylor
 - [ ] Async with Kevin
 - [ ] 1:1 with Jacki
 - [ ] Design Pair with Becka

### Tasks for Activation:
 - [ ] Finish up the illustration for [Email code verification when users meet high risk criteria](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/100)
 - [ ] Finish up [Send data based PQL when an email verification is not completed more than once for one or more users in 24 hours (If the namespace have 5 or more users and is free)](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/102) and [Send (PQL) email to admin when an email verification is not completed more than once for one or more users in 24 hours (If the namespace have 5 or more users)](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/101). 

### Tasks for Expansion:
 - [ ] Take a look at [UX: Trial wind down: in-product messaging and email ahead of trial end for namespaces over 5 user limit
](https://gitlab.com/gitlab-org/gitlab/-/issues/347441).

### Other Tasks:
 - [ ] Keep an eye on Arkose work
 
### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
