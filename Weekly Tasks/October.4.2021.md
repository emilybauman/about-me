## Week of October 4th to October 8th, 2021

## 14.4 Priorities
 - [Expansion Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/439)
 - [Activation Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/437)

### Key Meetings:
 - [ ] 1:1 with Jacki
 - [x] Coffee Chat with Anne
 - [x] Jacki's UX Team Meeting
 - [ ] Growth Weekly
 - [X] UX Weekly
 - [X] Growth Engineering Weekly
 - [x] Aquisitions Team Sync
 - [x] GitLab Welcome to Schwab
 - [x] Expansion Mid Milestone Catchup
 - [x] Reforge Kickoff
 - [x] Emily/Gayle Non-Admin Request Working Session
 - [ ] Emily/Andy Design Pair

 
### Tasks for Activation:
 - [X] Gather feedback for Research Plan on [Project Orchestration idea](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/418).
 - [x] Finish up first design concept for [Make user invitation (with invite for help) it's own dedicated step in first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/441).

### Tasks for Expansion:
 - [ ] Start research plan for [Problem Validation: How are non-invited users finding their team's namespace + groups/projects when they create new accounts?](https://gitlab.com/gitlab-org/gitlab/-/issues/340755)
 - [x] Confirm MVP and have a working session around [Experiment - Notify non-admin users to contact admin to invite new users to namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/337555)

### Other Tasks:
 - [ ] Start Research Plan for [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [x] Finish up Reforge lessons for the week including the Kickoff
 - [x] Add my thoughts to the [Retro for MR Design Reviews with Updated Guidelines](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1727)

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [x] Urdu Lessons
