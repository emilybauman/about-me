## Week of July 25th to July 29th, 2022

## 15.3 Priorities
- [UX Transition Plan for Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1998)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] Rayana & Emily 1:1
 - [x] Valerie/Emily B - 1:1
 - [x] Release Group Weekly [REC]
 - [x] UX Buddy session: Emily & Gina
 - [x] UX Weekly Call
 - [x] #random-coffees-ux-dept Donut
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Rayana & Emily 1:1
 - [x] Chris / Will / Rayana / Emily - Monthly Check-in
 - [x] Emily / Nadia ☕️
 
### Tasks for Release:
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Finalize first iterations of JTBDs and create some sub-jobs for [Task 2: Prepare 1-2 JTBDs for the Release Orchestration (RO) category.](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [ ] Pick up an issue or two from the [planning board](https://gitlab.com/groups/gitlab-org/-/boards/1489550?label_name%5B%5D=Next%20Up&label_name%5B%5D=devops%3A%3Arelease&label_name%5B%5D=group%3A%3Arelease). 
 - [x] Review some of the work for [Annotated Tags](https://gitlab.com/gitlab-org/gitlab/-/issues/362481#note_1033136306).

### Other Tasks:
 - [x] Finish up work on [redesigning the login page](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/91673). 
 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
