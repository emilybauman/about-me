## Week of November 21st, 2022 to November 25th, 2022

## 15.7 Priorities
- [15.7 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/174)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] UX Showcase
 - [x] Rayana & Emily 1:1

### Tasks for Release:
 - [x] Start research proposal and prototype for [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).
 - [x] Finish first iteration for [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).
 - [x] Finalize scenarios and organize data needed for sandbox for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [x] If time permits, start on design for [Give users more feedback when they have successfully created a release
](https://gitlab.com/gitlab-org/gitlab/-/issues/378924).
 - [x] If time permits, take a look at [Allow users to promote a deployment to it's own environment on the Environment Index Page
](https://gitlab.com/gitlab-org/gitlab/-/issues/378552).

### Other Tasks:
 - [x] Coverage tasks for Gina and Veethika.
 - [x] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [ ] Finish next task for Design Jam Session. 
 - [ ] Add insights to Dovetail for internail interviews. 

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
