## Week of April 25th, 2022 to April 29nd, 2022

## 15.0 Priorities
- Activation/Adoption Planning Issue: None for this Milestone 
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/629)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/628)

### Key Meetings:
 - [x] Jacki's UX Team (FG) Meetup
 - [x] UX Weekly Call
 - [x] Emily / Gayle/Sam Bi-Weekly
 - [x] UX Showcase
 - [x] Emily / Kevin - Sync
 - [x] ra_7692 Daily Stand Up (x2)

### Tasks for Activation:
 - [x] Continue work on [Email code verification when users meet high risk criteria](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/100)

### Tasks for Expansion:
 - [ ] Finish up work on [UX: alert banner group usage quota seats page](https://gitlab.com/gitlab-org/gitlab/-/issues/360171).
 - [ ] Take a look at [UX: Add premium trial CTA to revamped /billings page](https://gitlab.com/gitlab-org/gitlab/-/issues/357582).
 - [x] If time permits, take a look at [UX: Trial wind down: in-product messaging and email ahead of trial end for namespaces over 5 user limit
](https://gitlab.com/gitlab-org/gitlab/-/issues/347441) and [UX: Resolve remaining Awaiting user UI discrepancies on Group/Projects and Your Groups pages.
](https://gitlab.com/gitlab-org/gitlab/-/issues/356654).

### Other Tasks:
 - [ ] Finish Week 5 of Reforge Content.

### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
