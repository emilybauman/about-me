## Week of October 31st, 2022 to November 4th, 2022

## 15.6 Priorities
- [15.6 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/162)

### Key Meetings:
 - [x] Deployment experience with Andrei
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Sync for Interview
 - [x] Release Group Weekly [REC]
 - [x] CI/CD Skip-Level
 - [x] UX Weekly Call
 - [x] Interview
 - [x] Will/Emily Research Sync
 - [x] Russell/Emily 1:1
 - [x] Emily / Ali Environments page
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Release UX/PM sync
 - [x] Emily / Kevin - Sync
 - [x] Interview
 - [x] Notifications Design Jam - Sync Session 4

### Tasks for Release:
 - [ ] Clean up proposal for [Deploy freeze: show informative messages when deploy freeze is configured](https://gitlab.com/gitlab-org/gitlab/-/issues/212460).
 - [x] Clean up proposal for [View Deployment button disappears if environment URL is invalid](https://gitlab.com/gitlab-org/gitlab/-/issues/337304).
 - [x] Get first iteration done and plan for research for [Deployment Detail Page MVC - Deploy Approval](https://gitlab.com/gitlab-org/gitlab/-/issues/374538).
 - [ ] Get a first iteration done for [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).
 - [ ] Finish up Scenarios for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [x] Close off work for [Provide a mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab/-/issues/19724).
 - [x] If time allows, work on [Don't allow a deployment to be rerun after it's been approved](https://gitlab.com/gitlab-org/gitlab/-/issues/375512).
 - [ ] Plan for 15.7. 

### Other Tasks:
 - [ ] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [x] Lead two interviews. 
 - [x] Fill out interview scorecards. 
 - [x] Finish sketches for Notification Design Jam Session. 
 - [ ] Add insights to Dovetail for internail interviews. 

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
