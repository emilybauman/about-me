## Week of January 10th, 2022 to January 14th, 2022

## 14.7 Priorities
- [Activation Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/530)
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/528) this milestone.

### Key Meetings:
 - [x] Jacki's Team Meeting
 - [x] Growth Weekly
 - [x] UX Weekly
 - [x] 1:1 with Matej
 - [x] Aquisitions Team Meeting
 - [x] Free User Strategy Product/UX Sync
 - [x] Growth Conversion Team Meeting
 - [x] Async with Kevin
 - [x] 1:1 with Jacki
 - [ ] 1:1 with Andy

### Tasks for Activation:
 - [ ] Finish work on [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465).
 - [ ] Help with any final questions on [Test requiring credit card for all new namespaces](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/533)
 - [ ] Plan for next Milestone.

### Tasks for Expansion:
 - [ON HOLD] Start on [Indicate how many seats are remaining when namespace approaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/348694).
 - [ON HOLD] Start work on [Free user strategy: User Journey map](https://gitlab.com/gitlab-org/gitlab/-/issues/349704).
 - [ON HOLD] Finish work on [Create Free User Cap Feature flag and show in group settings](https://gitlab.com/gitlab-org/gitlab/-/issues/349811).
 - [ ] Plan for next Milestone.

### Other Tasks:
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
