## Week of September 27th to October 1st, 2021

## 14.4 Priorities
 - [Expansion Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/439)
 - [Activation Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/437)

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] UX Weekly
 - [x] Growth Weekly
 - [x] Aquisitions Team Sync
 - [x] UX Showcase
 - [x] 1:1 with Gayle
 - [x] 1:1 with Jensen
 - [x] 1:1 with Kevin
 - [x] 1:1 with Andy
 
### Tasks for Activation:
 - [x] Plan first research aspect of [Project Orchestration idea](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/418).
 - [ ] Start work on [Make user invitation (with invite for help) it's own dedicated step in first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/441).

### Tasks for Expansion:
 - [x] Finish up and get feedback on synthesis of [Allow non-admins to request access to a feature/tier or recommend an invite](https://gitlab.com/gitlab-org/ux-research/-/issues/1297).
 - [x] Finalize finishing touches on [Display invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/gitlab-org/gitlab/-/issues/336248).
 - [ ] Take a look at new research work around [Problem Validation: How are non-invited users finding their team's namespace + groups/projects when they create new accounts?](https://gitlab.com/gitlab-org/gitlab/-/issues/340755)
 - [x] Sync up with Gayle on next steps for [Experiment - Notify non-admin users to contact admin to invite new users to namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/337555)


### Other Tasks:
 - [ ] Plan tangible next steps for [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [x] Urdu Lessons
 
### Issue Backlog:

**To Be Done**

**Designs complete, waiting on implementation**
- [Experiment - Add expiration copy to invited user email](https://gitlab.com/gitlab-org/gitlab/-/issues/337860).
- [Problem validation: Invited user onboarding](https://gitlab.com/gitlab-org/ux-research/-/issues/1009).
- [Pitch a paid plan when users click to purchase CI minutes](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/420).
- [Provide admins with a sharable invite signup URL issue](https://gitlab.com/gitlab-org/gitlab/-/issues/336240/).
- [Add "welcome" questions to namespace creation UX](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/392).
- [Switch Easy-Button Icons with Radio Buttons](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/400).
- [Link CI Onboarding MR Widget to the Pipeline Editor](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/399).
- [Ask admin (inviter) what areas they'd like the invitee to focus on](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Notify inviter if invite email hard bounces](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Net Improvement - Fix Invite modal copy to accurately communicate "access expires" impact](https://gitlab.com/gitlab-org/gitlab/-/issues/337865). 
- [Add guided walkthrough to pipeline editor](https://gitlab.com/gitlab-org/gitlab/-/issues/334659)
