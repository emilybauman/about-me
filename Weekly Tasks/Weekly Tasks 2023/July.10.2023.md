## Week of July 10th, 2023 to July 14th, 2023

🌴 Short week with a PTO day on Friday, July 14th. 

## 16.2 Priorities
- [Environments 16.2 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/31)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] ☕ Hunter & Emily
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] PM Meeting (every 2 weeks) [REC]
 - [x] Emily / Ali
 - [x] UX Showcase
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review

### Tasks for Environments:
 - [ ] Finish screener and start on testing plan for [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [x] Look into 16.3 UX Planning. 
 - [x] Review takeaways from the Tech Discovery session and start putting together a prototype for the [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [x] Record Kickoff.

### Other Tasks:
 - [x] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Continue writing the blog post. 
 - [x] Present UX Showcase.
 - [x] Regroup with someone from UX around being a Design Sprint POC. 
