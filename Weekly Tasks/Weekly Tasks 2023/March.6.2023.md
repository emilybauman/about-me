## Week of March 6th, 2023 to March 10th, 2023

## 15.10 Priorities
- [Emily Bauman: Learn the Delivery domain tech](https://gitlab.com/gitlab-org/configure/general/-/issues/334)
- [Emily Bauman’s UX transition and onboarding to the Delivery stage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2210)
- [Configure 15.10 planning issue](https://gitlab.com/gitlab-org/configure/general/-/issues/331)
- [UX Transition for Configure](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2207)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] [REC] Refigure (Temporary Name) Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Discuss JTBDs for GitLab
 - [x] ☕ Tiger & Emily
 - [x] ☕ Pam & Emily
 - [x] Emily / Ali
 - [x] Rayana & Emily 1:1
 - [x] UX Showcase
 - [x] Refigure [Temp Name] - Design Pair Session
 - [x] Emily / Vladimir - coffee chat
 - [x] Customer Call 1

### Tasks for Refigure (temp name):
 - [ ] Finish reading [Terraform: Up & Running](https://www.terraformupandrunning.com/). 
 - [ ] Finalize research plan for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [ ] Complete [Kubernetes first steps course](https://learnk8s.io/first-steps).
 - [x] If time permits, take a look at [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901).
 - [x] Coffee chats with new team members. 
 - [x] Start organizing Configure and Release JTBDs. 

### Other Tasks:
 - [x] Change name from Senior Product Designer Release to Senior Product Designer Environments across the board.
 - [x] Watch Kuberentes Documentary. 
 - [x] Prep Design Pair Session.
 - [x] Finalize 15.11 Plan.
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
