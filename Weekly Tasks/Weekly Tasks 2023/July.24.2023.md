## Week of July 24th, 2023 to July 28th, 2023

## 16.3 Priorities
- [Environments 16.3 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/32)

### Key Meetings
 - [x] flux k8s dashboard chat
 - [x] Emily / Viktor weekly
 - [x] Emily / Vladimir - coffee chat
 - [x] Emily/Veethika Sync
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1 
 - [x] UX Showcase
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review

### Tasks for Environments:
 - [ ] Finish designs for [Design the external job visuals](https://gitlab.com/gitlab-org/gitlab/-/issues/417141).
 - [x] Summarize outcomes of [Internal research on populating a service list](https://gitlab.com/gitlab-org/gitlab/-/issues/418208).
 - [x] If time permits, look into [Agent status error communication](https://gitlab.com/gitlab-org/gitlab/-/issues/357344).
 - [x] Template out Design Sprint Artifacts. 

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish first draft of [Blog Post](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34383). 
 - [x] Regroup with someone from UX around being a Design Sprint POC. 
