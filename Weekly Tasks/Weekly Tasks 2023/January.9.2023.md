## Week of January 9th, 2023 to January 13th, 2023

- Slightly Short Week: Family and Friends day on Friday, January 13th

## 15.8 Priorities
- [15.8 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/202)

### Key Meetings:
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Release Group Weekly [REC]
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] Release UX/PM sync
 - [x] Reuben - CMS Environment Management
 - [x] Release UX/FE sync
 - [x] Mayra - CMS Environment Management
 - [x] Alessio - CMS Environment Management
 - [x] Emily / Vladimir - coffee chat
 - [x] Rayana & Emily 1:1
 - [ ] Emily & Gina sync
 - [ ] Michael / Emily - Design Pair

### Tasks for Release:
 - [ ] Finish proposal for [Add Search, Filter, Sort capabilities on the Environment history UI](https://gitlab.com/gitlab-org/gitlab/-/issues/372385).
 - [x] Finish sourcing participants and finish 3 interviews for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2121).
 - [x] Launch first version of study for [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).
 - [ ] Finish proposal for [Create Release Icon](https://gitlab.com/gitlab-org/gitlab-svgs/-/issues/342).
 - [ ] If time permits, take a look at [Prevent environment approvals rules from being removed after deployer/approver group is been deleted
](https://gitlab.com/gitlab-org/gitlab/-/issues/381877).

### Other Tasks:
 - [x] [Notify team of PTO in February](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2186).
 - [x] [Finish planning for 15.9](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/203).

### Personal Goals:
 - [ ] [Start looking into FY24 Goals](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
