## Week of April 3rd, 2023 to April 7th, 2023

🌴 Slightly shorter week - OOO on Friday, April 7th for Good Friday. 

## 15.11 Priorities
- [Emily Bauman: Learn the Delivery domain tech](https://gitlab.com/gitlab-org/configure/general/-/issues/334)
- [Emily Bauman’s UX transition and onboarding to the Delivery stage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2210)
- [Configure 15.11 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/1)

### Key Meetings
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Emily / Ali
 - [x] Emily & Gina sync
 - [x] UX Showcase
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily / Vladimir - coffee chat
 - [x] ☕ Hunter & Emily

### Tasks for Environments:
 - [ ] Finish research plan and screener for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [x] Get the first iteration of [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901) done.
 - [x] Take a look at [Show who is approved to deploy on the environment page and the approval rules](https://gitlab.com/gitlab-org/gitlab/-/issues/383411).

### Other Tasks:
 - [x] Continue with Onboarding.
 - [x] Prep Design Pair Session.
 - [x] Start UX Planning for 16.0.
 - [x] Prep OOO Issue for May.
 - [x] Add UX Section to planning issue template.
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
