## Week of March 13th, 2023 to March 17th, 2023

## 15.10 Priorities
- [Emily Bauman: Learn the Delivery domain tech](https://gitlab.com/gitlab-org/configure/general/-/issues/334)
- [Emily Bauman’s UX transition and onboarding to the Delivery stage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2210)
- [Configure 15.10 planning issue](https://gitlab.com/gitlab-org/configure/general/-/issues/331)
- [UX Transition for Configure](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2207)

### Key Meetings
 - [x] ☕ Timo & Emily
 - [x] LIVE | GitLab Q4 - Fiscal 2023 Earnings Call
 - [x] Emily / Viktor weekly
 - [x] [REC] Refigure (Temporary Name) Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] ☕ Phillip / Emily
 - [x] Emily / Ali
 - [x] Rayana & Emily 1:1
 - [x] Emily & Gina sync
 - [x] CI/CD UX Meeting
 - [x] [REC] Environments - Design Pair Session
 - [ ] Customer Call 1
 - [ ] Customer Call 2

### Tasks for Refigure (temp name):
 - [ ] Finish tasks in [Emily Bauman: Learn the Delivery domain tech](https://gitlab.com/gitlab-org/configure/general/-/issues/334).
 - [ ] Take a look at research plan for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [x] If time permits, take a look at [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901).
 - [x] Coffee chats with new team members. 
 - [x] Record 15.11 Kickoff Video. 
 - [x] Clean up [Sorting discrepancies for new environment page](https://gitlab.com/gitlab-org/gitlab/-/issues/391220).
 - [x] Clean up [Deployment Approvals should show automatically in the UI when a user clicks approve](https://gitlab.com/gitlab-org/gitlab/-/issues/388281).

### Other Tasks:
 - [x] Organize Figma Files for Release and Configure.
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
