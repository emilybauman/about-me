## Week of July 3rd, 2023 to July 7th, 2023

🌴 Short week with Canada Day on July 3rd and Family & Friends Day on July 4th.

## 16.2 Priorities
- [Environments 16.2 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/31)

### Key Meetings
 - [x] Rayana & Emily 1:1
 - [x] Emily / Ali
 - [x] CI/CD UX Meeting
 - [x] [REC] Tech Discovery: Service Oriented Operations
 - [x] Emily / Kevin - Sync
 - [x] Paycom/GitLab Environments Filtering Product Discussion
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily / Viktor weekly

### Tasks for Environments:
 - [ ] Finish screener and start on testing plan for [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [x] Look into 16.3 UX Planning. 
 - [x] Review takeaways from the Tech Discovery session and start putting together a prototype for the [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).

### Other Tasks:
 - [x] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Start writing the blog post. 
 - [x] Gather feedback on the UX Showcase Presentation. 
