## Week of December 25th, 2023 to December 29th, 2023

I am out of office enjoying some holiday time with my family. 🎄

Please contact my manager, [Rayana](https://gitlab.com/rayana) for anything urgent.
I will be back on Tuesday, January 2nd, 2024.
