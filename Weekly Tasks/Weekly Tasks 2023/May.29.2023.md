## Week of May 29th, 2023 to June 2nd, 2023

## 16.1 Priorities
- [Environments 16.1 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/23)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1 
 - [x] Emily / Ali
 - [x] UX Showcase
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [x] HOLD: Product All-Team Meeting [AMER] [REC] (Quarterly)

### Tasks for Environments:
 - [x] Continue recruiting participants for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326) - if any are available.
 - [x] Continue planning for the [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [x] Finish [Add loading state the environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/375247).

### Other Tasks:
 - [x] Catch up from PTO.
 
### Personal Goals:
 - [ ] Finish first iteration of the Individual Growth Plan. 
