## Week of September 4th, 2023 to September 8th, 2023

🌴 Short week with Labour Day on September 4th and PTO on September 5th and 6th.

## 16.4 Priorities
- [Environments 16.4 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/34)

### Key Meetings
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [x] HOLD: Product All-Team Meeting [AMER] [REC] (Quarterly)
 - [x] FY24-Q2 post-Earnings Internal Recap & AMA
 - [x] Taka | Emily Coffee Chat
 
### Tasks for Environments:
 - [ ] Recruit users for [Solution Validation: External Research on Populating a Service List](https://gitlab.com/gitlab-org/ux-research/-/issues/2577) and conduct some interviews.
 - [x] Start working on MVC Designs for [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [x] Finish concept for [Navigation to Kubernetes Dashboard with Agent Connection Issues](https://gitlab.com/gitlab-org/gitlab/-/issues/422443).

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [x] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
 - [x] Review 360 Feedback. 
