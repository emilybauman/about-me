## Week of June 12th, 2023 to June 16th, 2023

## 16.1 Priorities
- [Environments 16.1 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/23)

### Key Meetings
 - [x] Rayana & Emily 1:1
 - [x] Customer Call
 - [x] Design Sprint Day 3 Sync Session
 - [x] Emily / Viktor weekly
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] Customer Call
 - [x] Emily / Vladimir - coffee chat
 - [x] Emily / Ali
 - [x] Emily / Nico Coffee Chat
 - [x] ☕ Timo & Emily
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [x] Rayana/Emily IGP Conversation

### Tasks for Environments:
 - [x] Continue recruiting and interviewing participants for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326) - if any are available.
 - [x] Host and participate in Day 3 of the [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [ ] Finish [Add better feedback when starting a manual deployment action](https://gitlab.com/gitlab-org/gitlab/-/issues/388950).
 - [ ] Start recruiting users for the User testing portion of [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [x] Summarize user interviews. 

### Other Tasks:
 - [x] Film Kickoff for 16.2
 - [x] Finish first iteration of IGP.
 - [x] Send off participant re-imbursement. 
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
