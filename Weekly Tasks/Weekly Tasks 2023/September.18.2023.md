## Week of September 18th, 2023 to September 22nd, 2023

## 16.4 Priorities
- [Environments 16.4 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/34)

🌴 Short week with Medical PTO Half of the 19th and 20th as well as Family and Friends Day on the 22nd. 

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Environments JTBD Discussion
 - [x] Interview
 - [x] Customer Call
 - [x] Emily & Gina sync
 - [x] Rayana & Emily 1:1
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 
### Tasks for Environments:
 - [ ] Get first iteration done for [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [x] Start looking into [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [x] Make progress on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839).
 - [x] Share out findings from [Solution Validation: External Research on Populating a Service List](https://gitlab.com/gitlab-org/ux-research/-/issues/2577).

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
 - [x] Expense August Internet Bill. 
 - [x] Finish 16.5 Planning. 
 - [ ] Review recording for [UX Scorecard - Manage:Foundations FY24-Q3 - Author a pipeline: Look up reference](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2359).

