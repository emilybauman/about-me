## Week of June 27th to July 1st, 2022

- 🌴 Short Week - Canada Day on July 1st

## 15.2 Priorities
- Focus this Milestone on Transitioning into Release and Beautifying our UI
- [Career Mobility Emily Bauman, per 2022-06-13 as Product Designer](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/4157)
- [UX Transition Plan for Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1998)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] Coffee Chat ☕ Shinya/Emily
 - [x] Coffee Chat ☕ Vladimir/Emily
 - [x] Release Group Weekly [REC]
 - [x] ☕️ - Emily & Blair
 - [x] UX Buddy session: Emily & Gina
 - [x] UX Weekly Call
 - [x] Option 1: Post-Earnings GitLab Assembly (FY23-Q1 Results)
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Rayana & Emily 1:1
 - [x] Designer pair: Becka/ Emily
 
### Tasks for Release:
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Finish up the task [Task 1: Deploy a project and create a release using GitLab](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Finish up converting the [Experimentation Goal FY23](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to a Goal around Release.

### Tasks for Growth:
 - [x] Give feedback to user cap work when necessary.

### Other Tasks:
 - [x] Complete 2 MRs for [Beautifying Our UI 15.2](https://gitlab.com/gitlab-org/gitlab/-/issues/362122).
 - [x] Finalize [Beautifying our UI (15.2) - Final Designs](https://gitlab.com/gitlab-org/gitlab/-/issues/365763) to pass over to Jannik. 
 
### Personal Goals:
 - [x] Fix up [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
