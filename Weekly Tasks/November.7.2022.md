## Week of November 7th, 2022 to November 11th, 2022

## 15.6 Priorities
- [15.6 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/162)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] Skip Level: Christie and UX CI/CD
 - [x] UX Weekly Call
 - [x] Emily & Gina sync
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] Release Stage Brainstorm session
 - [x] Option 3: UX Holiday event: True or False game
 - [x] Option 3: UX retro gaming event
 - [x] Release PM/PD/TW sync

### Tasks for Release:
 - [x] Plan for research for [Deployment Detail Page MVC - Deploy Approval](https://gitlab.com/gitlab-org/gitlab/-/issues/374538).
 - [x] Get a first iteration done for [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).
 - [x] Finish up Scenarios and start looking at UX Sandbox for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [x] Close off [Don't allow a deployment to be rerun after it's been approved](https://gitlab.com/gitlab-org/gitlab/-/issues/375512).
 - [x] Plan for 15.7. 
 - [x] Understand what is left to close off [UX Theme: Provide efficient ways to be notified about deployments, so follow-up actions can be coordinated easily.
](https://gitlab.com/gitlab-org/gitlab/-/issues/369903).

### Other Tasks:
 - [x] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [ ] Finish next task for Design Jam Session. 
 - [x] Complete GitLab Survey.
 - [ ] Add insights to Dovetail for internail interviews. 

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
