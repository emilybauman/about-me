## Week of May 9th, 2022 to May 13th, 2022

## 15.0 Priorities
- Activation/Adoption Planning Issue: None for this Milestone 
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/629)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/628)

### Key Meetings:
 - [x] Skip Level: Christie and UX Enablement/Growth
 - [x] Emily / Jensen
 - [x] Jacki's UX Team (FG) Meetup [REC]
 - [x] UX Weekly Call
 - [x] Emily - Matej Async catchup
 - [x] Activation Team Sync
 - [x] Emily / Gayle/Sam Bi-Weekly
 - [x] Emily / Kevin - Sync
 - [x] 1:1 with Jacki
 - [x] Jensen/Emily Sync

### Tasks for Activation:
 - [x] Finish up the error states for [Email code verification when users meet high risk criteria](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/100)
 - [x] If time permits, work on [Send data based PQL when an email verification is not completed more than once for one or more users in 24 hours (If the namespace have 5 or more users and is free)](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/102) and [Send (PQL) email to admin when an email verification is not completed more than once for one or more users in 24 hours (If the namespace have 5 or more users)](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/101). 

### Tasks for Expansion:
 - [x] Finish off [UX: Add premium trial CTA to revamped /billings page](https://gitlab.com/gitlab-org/gitlab/-/issues/357582).
 - [ ] If time permits, take a look at [UX: Trial wind down: in-product messaging and email ahead of trial end for namespaces over 5 user limit
](https://gitlab.com/gitlab-org/gitlab/-/issues/347441).

### Other Tasks:
 - [x] Keep an eye on Arkose work
 
### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
