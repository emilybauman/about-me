## Week of July 11th to July 15th, 2022

- 🌴 Short Week - F&F Day on July 11th

## 15.2 Priorities
- Focus this Milestone on Transitioning into Release and Beautifying our UI
- [Career Mobility Emily Bauman, per 2022-06-13 as Product Designer](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/4157)
- [UX Transition Plan for Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1998)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] UX Buddy session: Emily & Gina
 - [x] T-Mobile & GitLab | Customer Fireside Chat
 - [x] UX Weekly Call
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Rayana & Emily 1:1
 - [x] Coffee Chat ☕ Kevin/Emily
 - [x] Designer pair: Becka/ Emily
 
### Tasks for Release:
 - [x] Catch up on pings while I was out on PTO.
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Get started on [Task 2: Prepare 1-2 JTBDs for the Release Orchestration (RO) category.](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).

### Other Tasks:
 - [ ] Complete 1 MR for [Beautifying Our UI 15.2](https://gitlab.com/gitlab-org/gitlab/-/issues/362122).
 - [x] Review any new MRs that come out of the initiative. 
 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
