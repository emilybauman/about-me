## Week of October 17th, 2022 to October 21st, 2022

## 15.6 Priorities
- [15.6 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/162)

### Key Meetings:
 - [x] Platform experience with Ahmed
 - [x] Interview
 - [x] Release Group Weekly [REC]
 - [x] UX Weekly Call
 - [x] Release UX/PM sync
 - [x] Emily & Gina sync
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [ ] Rayana & Emily 1:1
 - [ ] 1:1 Emily B and Jacki
 - [ ] Intro Chat
 - [ ] Michael / Emily - Design Pair
 
### Tasks for Release:
 - [ ] Get a first iteration done for [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).
 - [x] Validate JTBD for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [ ] Close off work for [Provide a mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab/-/issues/19724).
 - [ ] If time allows, tend to feedback on [Don't allow a deployment to be rerun after it's been approved](https://gitlab.com/gitlab-org/gitlab/-/issues/375512).

### Other Tasks:
 - [x] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [x] Finish interview scorecard.

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
