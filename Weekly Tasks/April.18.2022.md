## Week of April 18th, 2022 to April 22nd, 2022

## 15.0 Priorities
- Activation/Adoption Planning Issue: None for this Milestone 
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/629)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/628)

### Key Meetings:
 - [x] Growth Weekly
 - [x] UX Weekly Call
 - [x] 1:1 with Matej
 - [x] Growth Conversion Team Meeting
 - [x] Async with Kevin
 - [x] 1:1 with Jacki
 - [x] Design Pair with Becka

### Tasks for Activation:
 - [x] Finish off [Add walkthrough/wizard for the standard CI template](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/572).
 - [x] Start work on [Email code verification when users meet high risk criteria](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/100)

### Tasks for Expansion:
 - [x] Finish up work on [UX: Personal (user) namespace: Indicate how many seats are remaining when namespace approaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/358622), [UX: Personal (user) namespace: Lock invite options when namespace reaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/358624), [UX: Personal (user) namespace - When namespace reaches user limit alert Owners via banners and email](https://gitlab.com/gitlab-org/gitlab/-/issues/358625) and [UX: Personal (user) namespaces - For existing free namespaces over 5 users, notify Owner
](https://gitlab.com/gitlab-org/gitlab/-/issues/358627).
 - [ ] Take a look at [UX: Add premium trial CTA to revamped /billings page](https://gitlab.com/gitlab-org/gitlab/-/issues/357582)

### Other Tasks:
 - [x] Finish Week 4 of Reforge Content.

### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
