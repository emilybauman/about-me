## Week of March 7th, 2022 to March 11th, 2022

I am out of office on vacation in Saint Lucia. 🌴

- Please contact my manager, [Jacki](https://gitlab.com/jackib) for anything urgent.
- I will be back on **March 14th, 2022**.

