## Week of October 25th to October 29th, 2021

## 14.5 Priorities
 - [Expansion Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/447)
 - [Activation Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/455)

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] Growth Weekly
 - [x] UX Weekly
 - [x] Async with Matej
 - [x] Aquisition Team Sync
 - [x] UX Showcase
 - [x] 1:1 with Gayle
 - [x] 1:1 with Jensen
 - [x] 1:1 with Kevin
 - [x] Growth Design Sync
 - [x] Reforge: Monetization
 - [x] Design Pair with Andy

### Tasks for Activation:
 - [x] Finish up primary desktop design and start working on mobile version of [Create a marketing header when user is on GitLab and not logged in](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/448)
 - [x] Once the above is complete finalize up design for [Add trial signup to GitLab marketing header](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/449) (Not for MVP)
 - [x] Understand background information and overlap with Adoption's experiment for [Add "verify your identity" flow to first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/452)
 - [x] Wrap up work on [Add illustrations for survey response landing page](https://gitlab.com/gitlab-org/gitlab-svgs/-/merge_requests/732)
 - [x] Take a look at [When pipeline fails due to cc verification failure, Create a persistent header in the UI for that user](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/464) and [When pipeline fails due to cc verification failure, email the user with instructions](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/463) if time permits.

### Tasks for Expansion:
 - [x] Finish up template issue and close out work for [Non-admins can request to invite users via issue creation and assignment to namespace owner](https://gitlab.com/gitlab-org/gitlab/-/issues/340762)
 - [ ] Finish up Screener for Research around [How are non-invited users finding their team's namespace + groups/projects when they create new accounts?](https://gitlab.com/gitlab-org/gitlab/-/issues/340755)
 - [x] Take a look at [Area of Focus Iteration 2: Communicate selection to Admin selection(s) to Invitee](https://gitlab.com/groups/gitlab-org/-/epics/6897) if time permits. (Deprioritized)
 - [x] Work with Gayle to understand new Expansion Priorities with less engineering capacity.

### Other Tasks:
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [x] Finish up Reforge lessons for the week - Monetization - Growth Series

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] Urdu Lessons
