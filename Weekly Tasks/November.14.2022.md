## Week of November 14th, 2022 to November 18th, 2022
- Short week, taking PTO on Monday the 14th and Friday the 18th is Family & Friends Day. 

## 15.6 Priorities
- [15.6 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/162)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] PM Meeting (every 2 weeks) [REC]
 - [x] UX Weekly Call
 - [x] Record Kickoff Video
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] CEO AMA- Quarterly Kickoff
 - [x] Rayana & Emily 1:1
 - [x] Emily / Vladimir - coffee chat
 - [ ] Michael / Emily - Design Pair

### Tasks for Release:
 - [x] Finish up proposal for [Deployment Detail Page MVC - Deploy Approval](https://gitlab.com/gitlab-org/gitlab/-/issues/374538).
 - [x] Get a higher fidelity design done for [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).
 - [x] Get approval on scenarios and start creating the UX Sandbox for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [x] Plan for 15.7. 

### Other Tasks:
 - [x] Finish design for [Redesigned Login Page: Adjust Design for Longer Custom Text
](https://gitlab.com/gitlab-org/gitlab/-/issues/378004).
 - [ ] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [ ] Finish next task for Design Jam Session. 
 - [ ] Add insights to Dovetail for internail interviews. 

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
