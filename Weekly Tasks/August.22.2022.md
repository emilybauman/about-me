## Week of August 22nd, 2022 to August 26th, 2022

## 15.4 Priorities
- [15.4 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/153)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Chat about Design Sprints
 - [x] Release Group Weekly [REC]
 - [x] UX Buddy session: Emily & Gina
 - [x] UX Weekly Call
 - [x] Release UX/PM sync
 - [ ] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Harness Deploy approval walkthrough
 - [x] Michael / Emily - Design Pair
 - [ ] Notifications Design Jam - Session 1
 
### Tasks for Release:
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Get [Task 2: Prepare 1-2 JTBDs for the Release Orchestration (RO) category.](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/108916) merged.
 - [ ] Finish [UX Roadmap for Release](https://gitlab.com/gitlab-org/gitlab/-/issues/364527).
 - [x] Close off work for [Create annotated tags with Releases UI](https://gitlab.com/gitlab-org/gitlab/-/issues/362481).
 - [ ] Close off work for [Support multiple approval rules in project-level protected environments settings UI](https://gitlab.com/gitlab-org/gitlab/-/issues/367700).
 - [ ] Start on Prototype and questions for [Solution Validation: Validate how users want to be notified about pending approvals](https://gitlab.com/gitlab-org/ux-research/-/issues/2070).
 - [x] Continue work on [Support Approval Rules in Deployment Approval UI](https://gitlab.com/gitlab-org/gitlab/-/issues/355708).

### Other Tasks:
 - [x] Keep an eye out for shadowing opportunities based on [Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1864).
 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
