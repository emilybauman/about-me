## Week of February 10th, 2025 to February 14th, 2025

🌴 Slightly Short Week - OOO Monday

## 17.9 Priorities
- Duo Chat 
- Amazon Q

### Key Meetings
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Valerie / Emily
 - [x] [HOLD] Emily / Nathan AI Settings Sync
 - [x] Jackie / Emily Sync
 - [x] Emily / Libor (design pair)
 - [x] Emily / Pini catch up
 - [x] Torsten / Emily Sync
 - [x] Patrick / Emily Design Sync
 - [x] Data Science section UX meetup
 - [x] Emily / Jacki
  
### Tasks for Duo Chat:
 - [ ] Finalize Sandbox and Start Recruitment for [Solution Validation: Find-ability of Resizable Mechanism for Duo Chat Container](https://gitlab.com/gitlab-org/ux-research/-/issues/3344).
 - [x] Support [Composite Identity onboarding and offboarding flow](https://gitlab.com/gitlab-org/gitlab/-/issues/514388).
 - [x] Support [Phase 2: Create AI::Setting per group to opt-out of Anthropic Prompt Caching](https://gitlab.com/groups/gitlab-org/-/epics/16708).
 - [x] Support [Iteration 2: Multi-threaded conversations - List & CRUD (GitLab Web Page)](https://gitlab.com/groups/gitlab-org/-/epics/16108).

### Tasks for Q: 
 - [x] Finish [Add button for /fix quick action](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/365).
 - [x] Finish [Add UI disclaimer for AI generated in Code in merge requests](https://gitlab.com/gitlab-org/gitlab/-/issues/514790).
 
### Other Tasks:
 - [x] Finish first draft of [[Draft] Blog: ✨ Beautifying our UI (17.7-17.8) ✨](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/content-strategy-and-ops/blog/-/issues/293). 
 - [ ] Start on Design Sprint Planning. 
 - [x] Finish Trainings. 
