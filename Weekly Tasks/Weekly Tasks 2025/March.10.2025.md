## Week of March 10th, 2025 to March 14th, 2025

## 17.10 Priorities
- Duo Chat 
- Amazon Q

### Key Meetings
 - [x] Andrew & Emily Monthly ☕
 - [x] Duo Chat Weekly
 - [ ] Emily / Austin / Tim <> AI Design Pair
 - [x] Interview 1
 - [ ] Interview 2
 - [ ] Patrick / Emily Design Sync
 - [ ] Emily / Lina Design Pair
 - [ ] Jesse / Emily Onboarding Buddy
 - [ ] #ai-virtual-coffee Donut
 - [ ] ☕ Timo & Emily
 - [ ] Interview 3
 - [ ] #ai-virtual-coffee Donut
 - [ ] Torsten / Emily Sync
 - [ ] Emily / Pini catch up
 - [ ] Emily / Jacki
 - [ ] Data Science section UX meetup
 - [ ] Duo Sync: Emily/Taylor
 - [ ] Emily / Jay ☕️
 - [ ] Interview 4
 - [ ] Interview 5 

### Tasks for Duo Chat:
 - [ ] Finish interviews for [Solution Validation: Find-ability of Resizable Mechanism for Duo Chat Container](https://gitlab.com/gitlab-org/ux-research/-/issues/3344).
 - [ ] Finalize a proposal for [Duo Chat: allow editing the a question to course correct the conversation](https://gitlab.com/gitlab-org/gitlab/-/issues/477998).
 - [ ] Review draft of [DRAFT - Making Duo a trusted collaborator](https://gitlab.com/gitlab-org/gitlab/-/issues/523421).
 - [ ] Open issue for scrolling problems on Duo Chat. 
 - [ ] Review Design Sprint options with Torsten. 

### Tasks for Q: 
 - [ ] Support Patrick with UX Reviews. 
 - [ ] Support Health Check Reviews. 
 
### Other Tasks:
 - [ ] Start on Design Sprint Planning.
 - [ ] Finalize [[Draft] Coverage for Emily Bauman from 2024-03-17 until 2024-03-21](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2734).
 - [x] Open L&D issue for Conversational AI Training. 